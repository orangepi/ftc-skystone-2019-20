package org.firstinspires.ftc.teamcode.core;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class Hardware {

    //Names motors
    DcMotor FLDrive;
    DcMotor FRDrive;
    DcMotor BLDrive;
    DcMotor BRDrive;

    //Makes telemetry and HardwareMap usable in other classes.
    protected Telemetry telemetry;
    protected HardwareMap hardwareMap;

    public Hardware(Telemetry telemetry, HardwareMap hardwareMap, LinearOpMode linearOpMode) {
        this.telemetry = telemetry;
        this.hardwareMap = hardwareMap;
    }

    public Hardware(Telemetry telemetry, HardwareMap hardwareMap){
        this.telemetry = telemetry;
        this.hardwareMap = hardwareMap;
    }

    /**
     * Initiates Everything
     */
    public void initAllHardware(HardwareMap hardwareMap){
        FLDrive = hardwareMap.dcMotor.get("");
        FRDrive = hardwareMap.dcMotor.get("");
        BLDrive = hardwareMap.dcMotor.get("");
        BRDrive = hardwareMap.dcMotor.get("");
    }
}
