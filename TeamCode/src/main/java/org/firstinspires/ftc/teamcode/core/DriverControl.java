package org.firstinspires.ftc.teamcode.core;


import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;


@TeleOp(name = "TeleOp")

public class DriverControl extends OpMode {
    Hardware Hw = new Hardware(telemetry, hardwareMap);

    @Override
    public void init() {
        Hw.initAllHardware(hardwareMap);
        telemetry.addData("Started TeleOP", "1");
        telemetry.update();
    }

    @Override
    public void loop() {
        /**
         * The Controls for the Robot to the variables below are;
         * Move the left wheels =   LeftPower
         * Move the right wheels =  RightPower
         */
        double LeftPower = gamepad1.left_stick_y;
        double RightPower = gamepad1.left_stick_y;


        //Forward and Backward movement for left and right movement separately.
        Hw.FLDrive.setPower(LeftPower);
        Hw.BLDrive.setPower(LeftPower);

        Hw.FRDrive.setPower(RightPower);
        Hw.FRDrive.setPower(RightPower);

    }
}
